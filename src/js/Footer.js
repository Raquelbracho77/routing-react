import React from 'react';
import {Link} from 'react-router-dom';

const Footer = () => (
<footer>
	<nav arial-label="Secondary">
		<ul>
			<li>
				{/*<a href="/">H&#128147;me</a>*/}
				{/*
					Link to cumple la misma función que <a>
					Solo que es importado con la librería
					y se referencia a la otra pagina con un "/"
				*/}
				<Link 
					to="/" 
					style={{ textDecoration: 'none' }}
				>
					H&#128147;me
				</Link>
			</li>
			<li>
				<Link 
				to="/about" 
				style={{ textDecoration: 'none' }}
				>
					&#127829;About
				</Link>
			</li>
		</ul>
	</nav>
</footer>
);


export default Footer;