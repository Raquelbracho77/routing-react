import React from 'react';
import Section from './Section';

const About = () => (
	<Section headingText="About this weather app">
		<h1>About...</h1>
		<p>
			This application has been built with care and
			love by your <b>Ultimate Team!</b>
		</p>
	</Section>
);

export default About;