import React from 'react';
import {Link} from 'react-router-dom';
import Section from './Section';


const Home = () => (
	<Section headingText="Welcome to the weather report">
		<p>
			Please select one of the options below
			to view weather in your temperature scale of
			choice!
		</p>
		<nav aria-label="Main">
			<ul>
				<li>
					<Link to="/reports/kelvin" style={{ textDecoration: 'none' }}>
						View weather in Kelvin
					</Link>
				</li>
				<li>
					<Link to="/reports/celsius" style={{ textDecoration: 'none' }}>
						View weather in Celsius
					</Link>
				</li>
				<li>
					<Link to="/reports/fahrenheit" style={{ textDecoration: 'none' }}>
						View weather in Fahrenheit
					</Link>
				</li>
			</ul>
		</nav>
	</Section>
);


export default Home;