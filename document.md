## Estructura del proyecto 
~~~
/routing-react
	├── dist
	├── node_modules
	├── package.json
	├── package-lock.json
	└── src
	    ├── img
	    │   └── hom.png
	    ├── index.html
	    ├── js
	    │   ├── About.js
	    │   ├── App.js
	    │   ├── Footer.js
	    │   ├── Home.js
	    │   ├── index.js
	    │   ├── Report.js
	    │   ├── Section.js
	    │   └── weatherData.js
	    └── styles
	        └── temperatures.scss
~~~

## Instalar
~~~
npm install sass
~~~

##### Agregar `react-router` a la aplicación
~~~
npm install react-router-dom --save
~~~

##### [react-helmet](https://dev.to/olenadrugalya/what-is-react-helmet-and-where-to-use-it-2ed3), ayuda a administrar todos los cambios en el encabezado/`head` del documento. 
_Ver_: [Section.js](/src/js/Section.js)
~~~
npm install react-helmet --save
~~~

#### Notas
![Home](./src/img/hom.png)

Quitar el [subrayado](https://stackoverflow.com/questions/37669391/how-to-get-rid-of-underline-for-link-component-of-react-router) de `component Link` en `react-router`:
~~~
<Link to="/" style={{ textDecoration: 'none' }}>
~~~
*¿Cómo hacerlo dentro del script .scss?*


[Section.js](./src/js/Section.js) El método [.focus ()](https://medium.com/swlh/react-focus-c6ffd4aa42e5) le dice al navegador sobre qué elemento se está actuando, similar a .click () o un clic real.

#### Warnings y erorres:

`Warning: Each child in a list should have a unique "key" prop.` Agregar llave única a la etiqueta `<Fragment> ` en este caso.
~~~
<Fragment key={dataPoint.city}>
~~~

`Uncaught TypeError: dataPoint.temp is undefined`
Este error es muy común y se debe a la sintaxis (algúna palabra en el código mal escrita):
En el script donde están las temperaturas, [weatherData.js](./src/js/weatherData.js) de la ciudades, línea 20,
~~~
case 'fahrenheit':
		return data.map(({city, temp}) => ({
			city,
--------> tem: ((temp - 273.15) * 1.8 +32).toFixed(2),
~~~
Se encuentra el error, justo como se observa en consola. Al referenciar la ruta según el caso/`case` 
___http://localhost:1234/reports/fahrenheit___, fahrenheit muestra este error, como se observa en la 
[linea 20](https://gitlab.com/Raquelbracho77/routing-react/-/blob/master/src/js/weatherData.js#L20), es 'temp' y no tem, por eso dice que no está definido.
La sitanxis correcta es:
~~~
case 'fahrenheit':
		return data.map(({city, temp}) => ({
			city,
--------> temp: ((temp - 273.15) * 1.8 +32).toFixed(2),
~~~
Y al vincular nuevamente y recargar la página, no habrá error.

`undefined`, 
Por lo general,siempré será una variable mal escrita o alguna función no definida o con algún error en la sintaxis.


